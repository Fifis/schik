$(window).load(function () {

    $('#AV1').click(function () {
        if ($(this).is(':checked')) {
            $('#squeezespaces').removeAttr('checked');
            $('#punctum_space').removeAttr('checked');
            $('#sokr').removeAttr('checked');
            $('#fio').removeAttr('checked');
            $("#fdbrjcez").attr('checked', 'checked');
            $("#neg").attr('checked', 'checked');
            $("#parts").attr('checked', 'checked');
            $('#pretwo').removeAttr('checked');
            $('#adres').removeAttr('checked');
            $('#ref').removeAttr('checked');
            $('#comEI').removeAttr('checked');
            $('#trEI').removeAttr('checked');
            $('#dash').removeAttr('checked');
            $('#percent').removeAttr('checked');
            $("#dots").removeAttr('checked');
            $('#koe').removeAttr('checked');
            $('#blunder').removeAttr('checked');
            $('#AV3').removeAttr('checked');
            $('#AV4').removeAttr('checked');
        } else {
            //
        }
    });

    $('#AV3').click(function () {
        if ($(this).is(':checked')) {
            $('#squeezespaces').attr('checked', 'checked');
            $('#punctum_space').attr('checked', 'checked');
            $('#sokr').attr('checked', 'checked');
            $('#fio').attr('checked', 'checked');
            $("#fdbrjcez").attr('checked', 'checked');
            $("#neg").attr('checked', 'checked');
            $("#parts").attr('checked', 'checked');
            $('#pretwo').removeAttr('checked');
            $('#adres').attr('checked', 'checked');
            $('#ref').attr('checked', 'checked');
            $('#comEI').attr('checked', 'checked');
            $('#trEI').attr('checked', 'checked');
            $('#dash').attr('checked', 'checked');
            $('#percent').attr('checked', 'checked');
            $("#dots").attr('checked', 'checked');
            $('#koe').removeAttr('checked');
            $('#blunder').removeAttr('checked');
            $('#AV1').removeAttr('checked');
            $('#AV4').removeAttr('checked');
        } else {
            //
        }
    });

    $('#AV4').click(function () {
        if ($(this).is(':checked')) {
            $('#squeezespaces').attr('checked', 'checked');
            $('#punctum_space').attr('checked', 'checked');
            $('#sokr').attr('checked', 'checked');
            $('#fio').attr('checked', 'checked');
            $("#fdbrjcez").attr('checked', 'checked');
            $("#neg").attr('checked', 'checked');
            $("#parts").attr('checked', 'checked');
            $('#pretwo').removeAttr('checked');
            $('#adres').attr('checked', 'checked');
            $('#ref').attr('checked', 'checked');
            $('#comEI').attr('checked', 'checked');
            $('#trEI').attr('checked', 'checked');
            $('#dash').attr('checked', 'checked');
            $('#percent').attr('checked', 'checked');
            $("#dots").attr('checked', 'checked');
            $('#koe').attr('checked', 'checked');
            $('#blunder').attr('checked', 'checked');
            $('#AV1').removeAttr('checked');
            $('#AV3').removeAttr('checked');
        } else {
            //
        }
    });

});
