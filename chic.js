function activateCheckboxes() {
    if (document.getElementById("oTeX").checked) {
        document.getElementById("thspCompat").checked = false;
        document.getElementById("thspCompat").disabled = true;
        document.getElementById("addPreamble").disabled = false;
    } else {
        document.getElementById("thspCompat").disabled = false;
        document.getElementById("addPreamble").checked = false;
        document.getElementById("addPreamble").disabled = true;
    }
}

function deshitenize() {
    var in_text = document.papupa.gavgav.value;
    var ret = document.getElementById("meow");
    var th_nb_sp = "&amp;#8239;";
    var th_init_sp = "&amp;#8239;";
    var th_sp = "\\&amp;thinsp;"; // со слэшами, потому что используется при поиске регексов, а не при замене.
    var nb_sp = "&amp;nbsp;";
    var ru_dmdash = "&amp;mdash;&amp;thinsp;";
    var ru_pmdash = "&amp;mdash;";
    var ru_mdash = "&amp;#8239;&amp;mdash;&amp;thinsp;";
    var ru_ldots_t = "... ";
    var x_times = "&amp;times;";
    var ndash = "&amp;ndash;";
    var open_href = "<a href=\"";
    var mid_href = "\" target=\"_blank\">";
    var close_href = "</a>";
    var bbUTF = "(^\|[ \n\r\t.,'\"\+!?-«»“”„`’‹›]+)";
    var beUTF = "([ \n\r\t.,'\"\+!?-«»“”„`’‹›]+\|$)";
    if (document.getElementById("oTeX").checked) {
        th_nb_sp = "\\,";
        th_init_sp = "\",";
        th_sp = "\\\\,";
        nb_sp = "~";
        ru_dmdash = "\"--* ";
        ru_pmdash = "\"--~";
        ru_mdash = " \"--- ";
        ndash = "--";
        ru_ldots_t = "\\ldotst{} ";
        x_times = "$\\times$";
        open_href = "\\href{";
        mid_href = "}{";
        close_href = "}";
    }
    if (document.getElementById("oUTF").checked) {
        th_nb_sp = "&#8239;";
        th_init_sp = "&#8239;";
        th_sp = "\\&thinsp;";
        nb_sp = "&nbsp;";
        ru_dmdash = "&mdash;&thinsp;";
        ru_pmdash = "&mdash;";
        ru_mdash = "&#8239;&mdash;&thinsp;";
        ndash = "&ndash;";
        x_times = "&times;";
    }
    var str = "";
    str = repl(in_text, "[\u2009\u202F\u00A0]+", " ", true); // убиваем случайные юникодовские пробелы
    str = repl(str, "—\u00A0", "— ", true); // чистим тех, кто медленно отпускает AltGr после тире в типографской раскладке
    str = repL(str, "(?:(?:URL|Url|url): *)?(https?:/(?:/[a-z-A-Z_0-9.;%]+)+(?:/([a-z-A-Z_0-9.;%]+)))", open_href + "$1" + mid_href + "$2" + close_href, "hrefs");

    // УДАЛЕНИЕ ЛИШНИХ ПРОБЕЛОВ
    str = repl(str, "[\t\f ]+", " ", "squeezespaces");

    if (document.getElementById("oTeX").checked) {
        str = repl(str, "~ +", "~", "squeezespaces");
        str = repl(str, " +~", "~", "squeezespaces");
        str = repl(str, "~+", "~", "squeezespaces");
    }
    // и ещё надо искать что-то навроде «&nbsp; +», « +&nbsp;», «&nbsp;+»...

    // ЧИСТКА ПУНКТУАЦИИ — КАВЫЧКИ
    str = repL(str, "“([а-яёА-ЯЁ0-9A-Za-z])", "«$1", "punctum_space"); // “1861” → «1861», но нельзя, чтобы он заменял внутреннюю кавычку!
// Надо написать так, чтобы перед этой штукой не было ни знака "«", ни знака "„"
    str = repL(str, "”", "»", "punctum_space");
    str = repL(str, " \"([А-Яа-яЁёA-Za-z0-9,.?!;:() -]*) \"([А-Яа-яЁё ]*)\" *\"", " «$1 „$2“»", "punctum_space"); // Хитрый ход для четырёх кавычек
    str = repL(str, " \"([А-Яа-яЁёA-Za-z0-9,.?!;:() -]*) \"([А-Яа-яЁё ]*)\"", " «$1 „$2“»", "punctum_space"); // Хитрый ход для трёх кавычек
    str = repL(str, "\"([А-Яа-яЁё0-9])", "«$1", "punctum_space"); // "Красотка" → «Красотка»
    str = repL(str, "([А-ЯЁа-яё0-9!?.])\"", "$1»", "punctum_space");
// Не сработает, если перед этим стоит «и~». Надо учесть это во внутреннем синтаксисе!
    str = repL(str, "\"([ ,?!])", "»$1", "punctum_space"); // Грубое решение для тех, кто не прошёл предыдущий пункт
    str = repL(str, " \"", " «", "punctum_space");
    str = repL(str, "«([^»]*)«", "«$1„", "punctum_space"); // ( Имидж) → (Имидж)
    str = repL(str, "»([^«]*)»", "«$1„", "punctum_space"); // ( Имидж) → (Имидж)
    str = repL(str, " *([»)“])", "$1", "punctum_space"); // «Имидж » → «Имидж»
    str = repL(str, "([(«„]) *", "$1", "punctum_space"); // ( Имидж) → (Имидж)

    // ЧИСТКА ПУНКТУАЦИИ — ПРОЧЕЕ
    str = repL(str, "\\s+([,!:;.?])", "$1", "punctum_space"); // Я пришёл . → Я пришёл.
    str = repL(str, "([а-яё][.,:;?!]{1,5})([А-ЯЁ0-9])", "$1 $2", "punctum_space"); // Ночь.Улица → Ночь. Улица || AESh 2014-03-29:: Всё.453-им → Всё. 453-им
    str = repL(str, "([^\"»“”][,!;:?])([a-zA-ZА-Яа-яёЁ])", "$1 $2", "punctum_space");
    str = repL(str, " ([.,:;?!\\)]) ", "$1 ", "punctum_space");
    str = repL(str, " +- +(то|либо|нибудь|небудь)", "-$1", "dash");
    str = repL(str, "(где|как(?:ая|и(?:[емх]|ми)|о(?:го|[ей]|му)|ую)?|ке[йм]|ко(?:г(?:да|о)|му)|кто|куда|че(?:го|[йм]|му)|чьи[мх]?|чьими|чь[юя]) *- *(то|либо|нибудь|небудь)", "$1-$2", "dash");
    str = repL(str, "([А-ЯЁа-яё0-9A-Za-z])([(«])", "$1 $2", "punctum_space"); // оценка«отл.» → оценка «отл.»
    str = repL(str, "([)»])([А-ЯЁа-яё0-9A-Za-z])", "$1 $2", "punctum_space"); // «проолимпиадить»бюджет → «проолимпиадить» бюджет

    // ПРОБЕЛ В НЕРАЗРЫВНЫХ СОКРАЩЕНИЯХ
    str = repL(str, "[\\f\\t\\s ]+([гГвВ])[.]?[\\f\\t\\s ]*н[.]?[\\f\\t ]*[.]?э[.]?", nb_sp + "$1." + nb_sp + "н." + th_nb_sp + "э.", "sokr");
    str = repL(str, "[\\f\\t\\s ]+(год(а|у|ом|ы|ов|ам|ами|ах)?|век(а|у|ом|е|ов|ам|ами|ах)?)[.]?[\\f\\t\\s ]*н[.]?[\\f\\t ]*[.]?э[.]?", nb_sp + "$1" + nb_sp + "н." + th_nb_sp + "э.", "sokr");
    str = repL(str, "[\\f\\t\\s ]+([гГвВ])[.]?[\\f\\t\\s ]*до[\\f\\t\\s ]*н[.]?[\\f\\t ]*[.]?э[.]?", nb_sp + "$1. до" + nb_sp + "н." + th_nb_sp + "э.", "sokr");
    str = repL(str, "[\\f\\t\\s ]+(год(а|у|ом|ы|ов|ам|ами|ах)?|век(а|у|ом|е|ов|ам|ами|ах)?)[.]?[\\f\\t\\s ]*до[\\f\\t\\s ]*н[.]?[\\f\\t ]*[.]?э[.]?", nb_sp + "$1 до" + nb_sp + "н." + th_nb_sp + "э.", "sokr");
    str = repL(str, "[\\f\\t\\s ]*н[.][\\f\\t ]*э[.]*", nb_sp + "н." + th_nb_sp + "э.", "sokr");
    str = repL(str, "[\\f\\t\\s ]*н[\\f\\t ]*э[.]", nb_sp + "н." + th_nb_sp + "э.", "sokr");
    str = repL(str, "[\\f\\t\\s ]*н[.][\\f\\t ]*э([?!,:;])", nb_sp + "н." + th_nb_sp + "э.$1", "sokr");
    str = repL(str, "т[.]" + th_sp + "([педнкч])[.]", "т." + th_nb_sp + "$1.", "sokr");
    str = repL(str, "т[.][\\f\\t\\s ]*([педнкч])[.]", "т." + th_nb_sp + "$1.", "sokr");
    str = repL(str, "с[.][\\f\\t\\s ]*г[.]", "с[.]" + th_nb_sp + "г.", "sokr");

    // ПРОБЕЛ В ИНИЦИАЛАХ
    str = repL(str, "([А-ЯЁ]\\.) *([А-ЯЁ]\\.) *([А-ЯЁ][а-яёА-ЯЁ]\\.)", "$1" + th_init_sp + "$2" + th_init_sp + "$3", "fio");

    // ОДНОБУКВЕННЫЕ СЛОВА
    str = repL(str, "((?:^|[^а-яёА-ЯЁ])([~ ])?[авикосуяАВИКОСУЯ]) ", "$1" + nb_sp, "fdbrjcez");
    str = repL(str, "([~ ][авикосуяАВИКОСУЯ]) ", "$1" + nb_sp, "fdbrjcez");
    str = repL(str, "([ ~][вВсСкК])о ", "$1о" + nb_sp, "fdbrjcez");
    str = repL(str, "([ ~])об ", "$1об" + nb_sp, "fdbrjcez");

    // ОТРИЦАТЕЛЬНЫЕ ЧАСТИЦЫ
    str = repL(str, " ([нН][еи]) ", " $1" + nb_sp, "neg");
    str = repL(str, "~(н[еи]) ", nb_sp + "$1" + nb_sp, "neg");

    // ПОЛОЖИТЕЛЬНЫЕ ЧАСТИЦЫ
    str = repL(str, " (же?)([ ,.!?;:])", nb_sp + "$1$2", "parts");
    str = repL(str, " л([иь](?:[^а-яёА-ЯЁ]|$))", nb_sp + "л$1", "parts");
    str = repL(str, " (бы?)([ ,.!?;:])", nb_sp + "$1$2", "parts");

    // ДВУХБУКВЕННЫЕ ПРЕДЛОГИ
    str = repL(str, "( (?:[нНзЗ]а|[пПтТнНдД]о|[оО][бт]|[иИ]з)) ", "$1" + nb_sp, "pretwo");
    str = repL(str, "~([нз]а|[птнд]о|о[бт]|из) ", nb_sp + "$1" + nb_sp, "pretwo");

    // АДРЕС
    str = repL(str, "((?:^|[^а-яёА-ЯЁ])ул)[.]? +([А-ЯЁ])", "$1." + nb_sp + "$2", "adres");
    str = repL(str, "((?:^|[^а-яёА-ЯЁ])д|корп?|стр|кв|оф)[.]? *([0-9\\\\])", "$1." + nb_sp + "$2", "adres");

    // УКАЗАТЕЛИ
    str = repL(str, "((?:^|[^а-яёА-ЯЁ])(?:п|табл|(?:ри)?с|илл))[.]? *([0-9\\\\])", "$1." + nb_sp + "$2", "ref");
    str = repL(str, "([гГ]лав(?:[аыеу]|ами|ой|ах)?) ([0-9])", "$1" + nb_sp + "$2", "ref");
    str = repL(str, "(см[.]) ", "$1" + nb_sp, "ref");

    // ТЕХНИЧЕСКИЕ ЕИ
    str = repL(str, "([0-9])[\\f\\t ]*((?:мк?|[ГМТднк])?(?:dpi|[цНАБВстФ]|(?:ат|О)м|(?:би|бай|п|В)т|га?|Гц|Дж|Зв|кд|Кл|л[мк]?|м(?:оль)?|Па|пикс)(?:$|[^а-яёА-ЯЁ]))", "$1" + nb_sp + "$2", "trEI");

    // БЫТОВЫЕ ЕИ
    str = repL(str, "([0-9]) *(мл(?:н|рд)|тыс|коп|руб|раз|шт)", "$1" + nb_sp + "$2", "comEI");
    str = repL(str, "([0-9])[\\f\\t ]*(года?)", "$1" + nb_sp + "$2", "comEI");
    str = repL(str, "([0-9])[\\f\\t ]*(гг?[.])", "$1" + nb_sp + "$2", "comEI");
    str = repL(str, "([XIV])[\\f\\t ]*(вв?[.])", "$1" + nb_sp + "$2", "comEI");

    // ТИРЕ
    str = repL(str, "([0-9]) *- *([0-9])", "$1" + ndash + "$2", "dash"); // Надо выделить в отдельны пункт, ведь для ЛаТеХа это не годится, так как там минусы...
    str = repL(str, "([IVXDCM]) *- *([IVXDCM])", "$1" + ndash + "$2", "dash"); // Римские цифры обижать нельзя
    str = repL(str, "(?: |" + nb_sp + "|" + th_nb_sp + "|\\t|\\f)(?:&mdash;|-|–) ", " — ", "dash");
    str = repL(str, "^[\\f\\t ]*— ", ru_dmdash, "dash");
    str = repL(str, "[\\f\\t\\s ]*[—-] +", ru_mdash, "dash");
    str = repL(str, "[\\f\\t\\s ]*[—-](\r?\n)", ru_mdash + "$1", "dash");
    str = repL(str, "([а-яё])—(А-ЯЁ)", "$1" + ru_pmdash + "$2", "dash");
    str = repL(str, "([0-9]{3})(?:--|&ndash;|&amp;ndash;)([0-9]{2})(?:--|&ndash;|&amp;ndash;)?([0-9]{2})(?!(?:&nbsp;|~|&amp;nbsp;)г{1,2})", "$1-$2-$3$4", "dash"); // Номера телефонов

    // ЗНАКИ ПРЕПИНАНИЯ --- ПЕРЕНЕСТИ В ДРУГУЮ КАТЕГОРИЮ
//    str = repL(str, "\\?\\?", "$1" + ru_pmdash + "$2", "dash");
// Это был какой-то бред, что он делал?


    // ЗНАКИ ПРОЦЕНТА, УМНОЖЕНИЯ, НОМЕРА
    str = repL(str, "[ \u2009\u202F\u00A0]+%", th_nb_sp + "%", "percent");
    str = repL(str, "([0-9])%", "$1" + th_nb_sp + "%", "percent");
    str = repL(str, "([0-9]) *[хХxX*] *([0-9])", "$1" + th_nb_sp + x_times + th_nb_sp + "$2", "percent"); // Латинские «иксы» и русские «ха»
    str = repL(str, "([0-9])(.{0,6}) *\\* *(.{0,6})([0-9])", "$1$2" + th_nb_sp + x_times + th_nb_sp + "$3$4", "percent");
    str = repL(str, "№[ \u2009\u202F\u00A0]*([0-9])", "№"+th_nb_sp+"$1", "percent");
//    str = repL(str, "\u0023[ \u2009\u202F\u00A0]*([0-9])", "№"+th_nb_sp+"$1", "percent"); // #Решётка

    // МНОГОТОЧИЕ
    str = repL(str, " *… *", ru_ldots_t, "dots");

    // ДЕФИСЫ С КОЕ-
    str = repL(str, "([кК])о([ей])(где|как(?:ая|и(?:[емх]|ми)|о(?:го|[ей]|му)|ую)?|ке[йм]|ко(?:г(?:да|о)|му)|кто|куда|че(?:го|[йм]|му)|чьи[мх]?|чьими|чь[юя])", "$1о$2-$3", "koe");
    str = repL(str, "([кК])о([ей])[- ~]([а-я]+)[- ~](как(?:и(?:ми?|х)|о(?:го|му?))|[кч]ем|чь?е(му|го)|чь?ём|чь(?:е[йю]|и[хм]|ю))", "$1о$2" + nb_sp + "$3" + nb_sp + "$4", "koe");

    // ДЛЯ ДУРАКОВ
    str = repL(str, "-небудь", "-нибудь", "blunder");
    str = repL(str, "треть?[еия]я", "третья", "blunder");
    str = repL(str, "треть?[еия]ей", "третья", "blunder");
    str = repL(str, "трет[еия]ю", "третью", "blunder");
    str = repL(str, "трет[еия]ей", "третьей", "blunder");

    str = repL(str, "([0-9])-?т?ь?яя" + beUTF, "$1-я$2", "blunder"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?е?ей" + beUTF, "$1-й$2", "blunder"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ею" + beUTF, "$1-ю$2", "blunder"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?юю" + beUTF, "$1-ю$2", "blunder"); // Окончания числительных 3

    str = repL(str, "([0-9])-?т?ь?ее" + beUTF, "$1-е$2", "blunder"); // Окончания числительных 3

    str = repL(str, "([0-9])-?т?ии" + beUTF, "$1-и$2", "blunder"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?и?их" + beUTF, "$1-х$2", "blunder"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?и?им" + beUTF, "$1-м$2", "blunder"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?и?и?ми" + beUTF, "$1-ми$2", "blunder"); // Окончания числительных 3

    str = repL(str, bbUTF + "([иИ])з за" + beUTF, "$1$2з-за$3", "blunder"); // из-за
    str = repL(str, bbUTF + "([иИ])з под" + beUTF, "$1$2з-под$3", "blunder"); // из-под

    str = repL(str, bbUTF + "([яЯ]|[мМтТвВ]ы|он[а|о|и]?|[мМ]еня|[тТ]ебя|[нНвВ]ас|[нН]?[еЕ]го|[нН]?[еЕ][её]|[нН]?[иИ]х|[мМ]не|[тТ]ебе|[нНвВ]ам|[нН]?[еЕ]му|[нН]?[еЕ]й|[нН]?[иИ]м|[мМ]но[йю]|[тТ]обо[йю]|[нНвВ]ами|[нН]?[иИ]ми?|[нН]?[еЕ][йю]|[нН][её]м) *- *таки" + beUTF, "$1$2 таки$3", "blunder"); // убрать дефис после личных местоимений перед «таки»


    // РАСПИХАТЬ ПО КАТЕГОРИЯМ
    // АНАТОЛИЙ, НЕ УПРОЩАЙ РЕГЕКСЫ ДЛЯ ЧИСЛИТЕЛЬНЫХ, А ТО Я СДОХНУ
    str = repL(str, "([0-9])[ \u2009]([0-9]{3})", "$1" + th_nb_sp + "$2", "dash"); // Разряды в числах там, где пробел

    str = repL(str, "([0-9])-?[твн]?ый" + beUTF, "$1-й$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ого" + beUTF, "$1-го$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ому" + beUTF, "$1-му$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ым" + beUTF, "$1-м$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ом" + beUTF, "$1-м$2", "dash"); // Окончания числительных 1 4 1000

    str = repL(str, "([0-9])-?[твн]?ая" + beUTF, "$1-я$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ой" + beUTF, "$1-й$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ую" + beUTF, "$1-ю$2", "dash"); // Окончания числительных 1 4 1000

    str = repL(str, "([0-9])-?[твн]?ое" + beUTF, "$1-е$2", "dash"); // Окончания числительных 1 4 1000

    str = repL(str, "([0-9])-?[твн]?ые" + beUTF, "$1-е$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ых" + beUTF, "$1-х$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ым" + beUTF, "$1-м$2", "dash"); // Окончания числительных 1 4 1000
    str = repL(str, "([0-9])-?[твн]?ыми" + beUTF, "$1-ми$2", "dash"); // Окончания числительных 1 4 1000

    str = repL(str, "([0-9])-?[ртм]?ой", "$1-й", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ого", "$1-го", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ому", "$1-му", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ым", "$1-м", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ом", "$1-м", "dash"); // Окончания числительных 2 6 7

    str = repL(str, "([0-9])-?[ртм]?ая", "$1-я", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ой", "$1-й", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ую", "$1-ю", "dash"); // Окончания числительных 2 6 7

    str = repL(str, "([0-9])-?[ртм]?ое", "$1-е", "dash"); // Окончания числительных 2 6 7

    str = repL(str, "([0-9])-?[ртм]?ые" + beUTF, "$1-е$2", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ых" + beUTF, "$1-х$2", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ым" + beUTF, "$1-м$2", "dash"); // Окончания числительных 2 6 7
    str = repL(str, "([0-9])-?[ртм]?ыми" + beUTF, "$1-ми$2", "dash"); // Окончания числительных 2 6 7

    str = repL(str, "([0-9])-?т?ий" + beUTF, "$1-й$2", "dash"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?е?го" + beUTF, "$1-го$2", "dash"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?е?му" + beUTF, "$1-го$2", "dash"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?е?им" + beUTF, "$1-м$2", "dash"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?е?ем" + beUTF, "$1-м$2", "dash"); // Окончания числительных 3

    str = repL(str, "([0-9])-?т?ья" + beUTF, "$1-я$2", "dash"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?е[йю]" + beUTF, "$1-й$2", "dash"); // Окончания числительных 3, в т.ч. «третьею книгою»
    str = repL(str, "([0-9])-?т?ью" + beUTF, "$1-го$2", "dash"); // Окончания числительных 3

    str = repL(str, "([0-9])-?т?ье" + beUTF, "$1-е$2", "dash"); // Окончания числительных 3

    str = repL(str, "([0-9])-?т?ьи" + beUTF, "$1-и$2", "dash"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?их" + beUTF, "$1-х$2", "dash"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?им" + beUTF, "$1-м$2", "dash"); // Окончания числительных 3
    str = repL(str, "([0-9])-?т?ь?и?ми" + beUTF, "$1-ми$2", "dash"); // Окончания числительных 3


    if (document.getElementById("addPreamble").checked) {
        beginPreamble = "\\documentclass[10pt,a4paper]\{article\}\n\n\\usepackage\{cmap\}\n\\usepackage[T2A]\{fontenc\}\n\\usepackage[utf8]\{inputenc\}\n\\usepackage\{amsmath,amsfonts,amssymb,amsthm\}\n\\usepackage\{indentfirst,eurosym\}\n\\usepackage[russian]\{babel\}\n\\usepackage\{microtype\}\n\\usepackage[paper=a4paper]\{geometry\}\n\\usepackage\{xcolor\}\n\\usepackage[pdftex,unicode]\{hyperref\}\n\n\\righthyphenmin=2\n\n\\begin\{document\}\n";
        endPreamble = "\n\\end\{document\}";
        beginPreamble += str;
        beginPreamble += endPreamble;
        str = beginPreamble;
    }

    if (document.getElementById("oTeX").checked && document.papupa.bad.value.length) str = repl(str, "([" + document.papupa.bad.value + "])", "\\begin{verbatim}$1\\end{verbatim}", true);
    ret.innerHTML = str;
}

    // Функция похожа на что-то устаревшее. Надо проверить, привязывает ли основной типограф физические величины, как эта, и убрать эту вещь к чертям. Анатолий, ты её породил — ты её и убьёшь.
function AV4() {
    var in_text = document.papupa.gavgav.value;
    var ret = document.getElementById("meow");
    var str = "";
    str = repl(str, "([^\"»“”][,!;:?])([a-zA-ZА-Яа-яёЁ])", "$1 $2", true);
    str = repl(str, "([0-9])[\t\f ]*([кМГТдмн]|мк)?(м)([23])([^0-9]|$)", "$1~$2$3\\$^$4$5", true);
    str = repl(str, " ([.,:;?!\\)]) ", "$1 ", true);
    str = repl(str, "\"([а-яА-ЯёЁ])", "«$1", true);
    ret.innerHTML = repl(str, "([а-яА-ЯёЁ!?])\"", "$1»", true);
}


function repl(str, pat, sub, nado) {
    if (nado) {
        str = str.replace(RegExp(pat, "gm"), sub);
    }
    return str;
}

function repL(str, pat, sub, iD) {
    return repl(str, pat, sub, document.getElementById(iD).checked);
}
