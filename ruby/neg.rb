text = "♂dq♀My string ♂dq♀is♂dq♀ glorious.♂dq♀\n♂dq♀ My string ♂dq♀is♂dq♀ glorious.♂dq♀\n♂dq♀My string ♂dq♀ is♂dq♀ glorious.♂dq♀\n♂dq♀My string ♂dq♀is ♂dq♀ glorious.♂dq♀\n♂dq♀My string ♂dq♀is♂dq♀ glorious. ♂dq♀\n♂dq♀My string ♂dq♀i♂dq♀ glorious.♂dq♀\n♂dq♀ My string ♂dq♀i♂dq♀ glorious.♂dq♀\n♂dq♀My string ♂dq♀ i♂dq♀ glorious.♂dq♀\n♂dq♀My string ♂dq♀i ♂dq♀ glorious.♂dq♀\n♂dq♀My string ♂dq♀i♂dq♀ glorious. ♂dq♀\n\n♂dq♀My string ♂dq♀is♂dq♀♂c♀♂s♀he♂dq♀me♂dq♀"
nq = '(?:(?!♂dq♀).)'
nqs = '(?:(?!♂dq♀|♂s♀).)'
fin = "♂dq♀(#{nqs}#{nq}* )♂dq♀(#{nqs}(?:#{nq}*#{nqs})?)♂dq♀ (#{nq}*#{nqs})♂dq♀"
find = Regexp.new(fin)
replace = '«\1 “\2” \3»'
puts text.gsub(find, replace)
puts ""
puts '1: |'+$1+'|, 2: |'+$2+'|, 3: |'+$3+'|'
puts ""
puts fin.gsub(/#\{nqs\}/, nqs).gsub(/#\{nq\}/, nq)

