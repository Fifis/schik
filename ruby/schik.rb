# Input mode: KASchA, UTF-8 [default], HTML, TeX
# Output mode: KASchA, UTF-8 [default], HTML, TeX, restricted UTF-8

# Conversion of a file to KASchA
def input_to_kasha(im = 1, filename = "sample.txt")
  if im==0
    $mytext = File.readlines(filename).join("")
  else
    $mytext = File.readlines(filename).join("")
    $allsymbols.each { |x| 
      find = Regexp.new(Regexp.escape(x[im]))
      replace = x[0]
      $mytext = $mytext.gsub(find, replace)
    }
  end
end

# Conversion from KASchA to a file
def output_from_kasha(om = 1, filename = "y.txt")
  stuff = $mytext
  if om==0
    File.write(filename, stuff)
  else
    $allsymbols.each { |x| 
      find = Regexp.new(x[0])
      replace = x[om]
      stuff = stuff.gsub(find, replace)
    }
    File.write(filename, stuff)
  end
end

# The following array provides several variants for each typographic mark:
# $var = ["sym0", "sym1", "sym2", "sym3", "sym4", ...]
# $var[0] is the KASchA symbol name (for conversion and processing)
# $var[1] is the Unicode representation (for input and output)
# $var[2] is the HTML representation (for input and output)
# $var[3] is the TeX representation (for input and output)
# $var[4] is the “compatible” Unicode version (for output)
# $var[5] and greater are the alternative representations of LaTeX symbols,
# because e.g. both `` and “ will produce the opening double quote mark.

# TeX: 
# /\\male([^a-zA-Z])/☾m☽\1/
# /\\[mM]ars([^a-zA-Z])/☾m☽\1/
# /\\[vV]enus/☾f☽/
#/\\female/☾f☽/ 
# Искать UTF nbsp и заменять
# \\euro(\{\})? или принудительный пробел
# \\pounds(\{\})?
# \\og
# \\fg(\{\})?
# \\ldotst(\{\})?, q, e
# \\^(\{\})?
# \\~(\{\})?
# Менять '' до ' и `` до `
# Менять "---, "--*, "--~, потом ---, потом --
# Менять нижнюю одинарную кавычку на тупую
# Предупреждать (или даже заменять) в формулах трубу на \mid
# Предупреждать, что \textdegree требует textcomp (смотреть, есть ли в выводе \textdegree)
# Предупреждать, если обработчик не все \dq вычистил!

def set_repl
$male = ["☾m☽", "♂", "&#9794;", "\\male", "♂"]
$female = ["☾f☽", "♀", "&#9792;", "\\female", "♀"]
$s = ["♂s♀", " ", " ", " ", " ", "\\ "]
$tab = ["♂t♀", "\t", "\t", "\t", " "]
$d = ["♂d♀", ".", ".", ".", "."]
$c = ["♂c♀", ",", ",", ",", ","]
$col = ["♂col♀", ":", ":", ":", ":"]
$scol = ["♂scol♀", ";", ";", ";", ";"]
$e = ["♂e♀", "!", "!", "!", "!"]
$q = ["♂q♀", "?", "?", "?", "?"]
$dq = ["♂dq♀", "\"", "&quot;", "\\dq", "\""]
$dol = ["♂dol♀", "$", "$", "\\$", "$"]
$euro = ["♂euro♀", "€", "&euro;", "\\euro", "€"]
$pound = ["♂pound♀", "£", "&pound;", "\\pounds", "£"]
$pc = ["♂pc♀", "%", "%", "\\%", "%"]
$amp = ["♂amp♀", "&", "&amp;", "\\&", "&"]
$at = ["♂at♀", "@", "@", "@", "@"]
$da = ["♂da♀", "'", "'", "'", "'"]
$lp = ["♂lp♀", "(", "(", "(", "("]
$rp = ["♂rp♀", ")", ")", ")", ")"]
$lbk = ["♂lbk♀", "[", "[", "[", "["]
$rbk = ["♂rbk♀", "]", "]", "]", "]"]
$lbc = ["♂lbc♀", "{", "{", "\\{", "{"]
$rbc = ["♂rbc♀", "}", "}", "\\}", "}"]
$lg = ["♂lg♀", "«", "&laquo;", "«", "«", "<<", "\\og", "\\guillemotleft"] #
$rg = ["♂rg♀", "»", "&raquo;", "»", "»", ">>", "\\fg", "\\guillemotright"] #
$lgs = ["♂lgs♀", "‹", "&lsaquo;", "\\guilsinglleft", "<"]
$rgs = ["♂rgs♀", "›", "&rsaquo;", "\\guilsinglright", ">"]
$ld = ["♂ld♀", "“", "&ldquo;", "``", "“", "“"] #
$rd = ["♂rd♀", "”", "&rdquo;", "''", "”", "”"] #
$la = ["♂la♀", "„", "&bdquo;", ",,", "„", "„"] #
$ra = ["♂ra♀", "“", "&rdquo;", "``", "“", "“"] #
$ls = ["♂ls♀", "‘", "&lsquo;", "‘", "‘"]
$rs = ["♂rs♀", "’", "&rsquo;", "’", "’"]
$sbquo = ["♂sbquo♀", "‚", "&sbquo;", "‚", "‚"]
$ast = ["♂ast♀", "*", "*", "*", "*"]
$plus = ["♂plus♀", "+", "+", "+", "+"]
$h = ["♂h♀", "-", "-", "-", "-"]
$minus = ["♂minus♀", "−", "&minus;", "$-$", "-"]
$nd = ["♂nd♀", "–", "&ndash;", "--", "–"]
$md = ["♂md♀", "—", "&mdash;", "---", "—"]
$sl = ["♂sl♀", "/", "/", "/", "/"]
$bsl = ["♂bsl♀", "\\", "\\", "\\textbackslash", "\\"]
$eq = ["♂eq♀", "=", "=", "=", "="]
$gt = ["♂gt♀", ">", "&gt;", "$>$", ">"]
$lt = ["♂lt♀", "<", "&lt;", "$<$", "<"]
$ge = ["♂ge♀", "≥", "&ge;", "$\\ge$", "≥", "$\\geq$"]
$le = ["♂le♀", "≤", "&le;", "$\\le$", "≤", "$\\leq$"]
$geqslant = ["♂geqslant♀", "⩾", "&ge;", "$\\geqslant$", "≥"]
$leqslant = ["♂leqslant♀", "⩽", "&le;", "$\\leqslant$", "≤"]
$ldots = ["♂ldots♀", "…", "&hellip;", "\\ldots", "..."]
$hat = ["♂hat♀", "^", "^", "\\textasciicircum", "^", "\\^{}"]
$us = ["♂us♀", "_", "_", "\\_", "_"]
$ca = ["♂ca♀", "’", "&apos;", "’", "’"]
$acute = ["♂acute♀", "´", "&acute;", "\\'{}", "´"]
$grave = ["♂grave♀", "`", "`", "\\`{}", "`"]
$pipe = ["♂pipe♀", "|", "|", "|", "|"]
$numero = ["♂numero♀", "№", "№", "№", "№"]
$number = ["♂number♀", "#", "#", "\\#", "#"]
$div = ["♂div♀", "÷", "&divide;", "$\\div$", "/"]
$tilde = ["♂tilde♀", "~", "&tilde;", "\\textasciitilde", "~", "\\~{}"]
$nbsp = ["♂nbsp♀", " ", "&nbsp;", "~", " "]
$thsp = ["♂thsp♀", " ", "", "", " "]
$thnb = ["♂thnb♀", " ", "", "", " "]
$deg = ["♂deg♀", "°", "&deg;", "\\textdegree", "°"]
$pm = ["♂pm♀", "±", "&plusmn;", "\\pm", "±"]
$times = ["♂times♀", "×", "&times;", "$\\times$", "×"]
$cdot = ["♂cdot♀", "·", "&middot;", "$\\cdot$", "·"]
$bullet = ["♂bullet♀", "•", "&bull;", "$\\bullet$", "•"]
$prime = ["♂prime♀", "′", "&prime;", "${}'$", "'"]
$pprime = ["♂pprime♀", "″", "&Prime;", "${}''$", "''"]
$sect = ["♂sect♀", "§", "&sect;", "\\S", "§"]
$copy = ["♂copy♀", "©", "&copy;", "\\textregistered", "(C)"]
$reg = ["♂reg♀", "®", "&reg;", "\\textcopyright", "(R)"]
$tm = ["♂tm♀", "™", "&trade;", "\\texttrademark", "(TM)"]
$shy = ["♂shy♀", "­", "&shy;", "\\-", ""]
###### Special KASchA declarations
$fs = ["♂fs♀", ".", ".", ".", "."]
$ct = ["♂ct♀", ".", ".", ".", "."]
$ds = ["♂ds♀", ",", ",", "{,}", ","]
$datesep = ["♂datesep♀", ".", ".", ".", "."]
$rumd = ["♂rumd♀", " — ", "&#8239;&mdash;&thinsp;", " \"--- ", " — "]
$rujd = ["♂rujd♀", "—", "&mdash;", "\"--~", "—"]
$rudd = ["♂rudd♀", "— ", "&mdash;&nbsp;", "\"--* ", "— "]
$ntsp = ["♂ntsp♀", " ", "&#8239;", "\",", " "]
$lsotst = ["♂ldotst♀", "...", "...", "\\ldotst{}", "..."]
$ldotsq = ["♂ldotsq♀", "?..", "?..", "\\ldotsq{}", "?.."]
$ldotse = ["♂ldotse♀", "!..", "!..", "\\ldotse{}", "!.."]
$lqi = ["♂lqi♀", "«", "&laquo;", "«", "«"]
$rqi = ["♂rqi♀", "»", "&raquo;", "»", "»"]
$lqii = ["♂lqii♀", "„", "&rdquo;", "„", "„"]
$rqii = ["♂rqii♀", "“", "&bdquo;", "“", "“"]
$lqiii = ["♂lqiii♀", "‘", "&lsquo;", "‘", "‘"]
$rqiii = ["♂rqiii♀", "’", "&rsquo;", "’", "’"]
$nbh = ["♂nbh♀", "-", "-", "~=", "-"]
$bh = ["♂bh♀", "-", "-", "\"=", "-"]
$dgsep = ["♂dgsep♀", " ", "&#8239;", "\\,", " "]
$takdal = ["♂takdal♀", "т. д.", "т.&#8239;д.", "т.\\,д.", "т. д."]
$tompod = ["♂tompod♀", "т. п.", "т.&#8239;п.", "т.\\,п.", "т. п."]
$taknaz = ["♂taknaz♀", "т. н.", "т.&#8239;н.", "т.\\,н.", "т. н."]
$takkak = ["♂takkak♀", "т. к.", "т.&#8239;к.", "т.\\,к.", "т. к."]
$toest = ["♂toest♀", "т. е.", "т.&#8239;е.", "т.\\,е.", "т. е."]
$tomchi = ["♂tomchi♀", "т. ч.", "т.&#8239;ч.", "т.\\,ч.", "т. ч."]
$nery = ["♂nery♀", "н. э.", "н.&#8239;э.", "н.\\,э.", "н. э."]
$donery = ["♂donery♀", "до н. э.", "до&nbsp;н.&nbsp;э.", "до~н.\\,э.", "до н. э."]
$sgoda = ["♂sgoda♀", "с. г.", "с.&#8239;г.", "с.\\,г.", "с. г."]
$nbspi = ["♂nbspi♀", " ", "&nbsp;", "~", " "]
$nbspii = ["♂nbspii♀", " ", " ", " ", " "]
$nbspiii = ["♂nbspiii♀", " ", " ", " ", " "]
end

# Requires textcomp: \textdegree
# Requires special declarations: \ldotst, \ldotse. \ldotsq

set_repl

$allsymbols = [$male, $female, $s, $tab, $d, $c, $col, $scol, $e, $q, $dq, $dol, $euro, $pound, $pc, $amp, $at, $da, $lp, $rp, $lbk, $rbk, $lbc, $rbc, $lg, $rg, $lgs, $rgs, $ld, $rd, $la, $ra, $ls, $rs, $sbquo, $ast, $plus, $h, $minus, $nd, $md, $sl, $bsl, $eq, $gt, $lt, $ge, $le, $geqslant, $leqslant, $ldots, $hat, $us, $ca, $acute, $grave, $pipe, $numero, $number, $div, $tilde, $nbsp, $thsp, $thnb, $deg, $pm, $times, $cdot, $bullet, $prime, $pprime, $sect, $copy, $reg, $tm, $shy, $fs, $ct, $ds, $datesep, $rumd, $rujd, $rudd, $ntsp, $lsotst, $ldotsq, $ldotse, $lqi, $rqi, $lqii, $rqii, $lqiii, $rqiii, $nbh, $bh, $dgsep, $takdal, $tompod, $taknaz, $takkak, $toest, $tomchi, $nery, $donery, $sgoda, $nbspi, $nbspii, $nbspiii]

# Это никуда не годится:
# $needstexescape = [$dq, $euro]
# Надо смотреть if im==3 и с самого начала прогонять замены навроде
# /\\textasciitilde\{\}/ --> $tilde[0]
# /\\textasciitilde\\ / --> $tilde[0]$s[0]
# В конце опять убиваем двойные пробелы.

input_to_kasha(1, "sample.txt")
output_from_kasha(0, "y.txt")

def br (str = "")
  return '('+str+')'
end

$otherspaces = $nbsp[0]+'|'+$tab[0]+'|'+$thsp[0]+'|'+$thnb[0] # Non-standard spaces
$spaces = $s[0]+'|'+$otherspaces # All spaces
$anypunct = $d[0]+'|'+$c[0]+'|'+$col[0]+'|'+$scol[0]+'|'+$e[0]+'|'+$q[0]+'|'+$lp[0]+'|'+$rp[0]+'|'+$lbk[0]+'|'+$rbk[0]+'|'+$lbc[0]+'|'+$rbc[0]+'|'+$h[0]+'|'+$ast[0]+'|'+$nd[0]+'|'+$md[0]+'|'+$sl[0]+'|'+$bsl[0]+'|'+$ldots[0]+'|'+$hat[0]+'|'+$us[0]+'|'+$pipe[0]+'|'+$numero[0]+'|'+$number[0]+'|'+$div[0]+'|'+$tilde[0]+'|'+$deg[0]+'|'+$pm[0]+'|'+$times[0]+'|'+$cdot[0]+'|'+$bullet[0]+'|'+$prime[0]+'|'+$pprime[0]+'|'+$shy[0]
$anyletter = "[а-яА-ЯёЁA-Za-z0-9]"
$punctorspace = $anypunct+'|'+$s[0]
$anynonqusp = $anypunct+'|'+$anyletter
$anynonsp = "(?:(?!#{$s[0]}).)"
$anynondqu = "(?:(?!#{$dq[0]}).)"
$anynonqu = "(?:(?!#{$dq[0]}|#{$lg[0]}|#{$lqi[0]}|#{$rg[0]}|#{$rqi[0]}|#{$lqii[0]}|#{$rqii[0]}).)"

def timer
  $starttime = Time.now
end

def showtime(msg = "Time taken")
  $endtime = Time.now - $starttime
  printf("%s: %1.6f\n", msg, $endtime)
end

# Elimination of excess white space
def squeeze_spaces
  find = Regexp.new(br($otherspaces)+'+')
  replace = $s[0]
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("Converting spaces")
  find = Regexp.new(br($s[0])+'+')
  replace = $s[0]
  start = Time.now
  $mytext = $mytext.gsub(find, replace)
  showtime("Squeezing spaces")
end

# Elinimation of spaces at the beginning of each line
def initial_spaces
  find = Regexp.new('^'+br($spaces)+'+')
  replace = ""
  $mytext = $mytext.gsub(find, replace)
end

def quotemarks
  # 1. Save good pairs like «this „way“» as the final version
  # Non-greedy because the first consequent occurrence is the desired one
  find = Regexp.new($lg[0]+'(.*?)'+$la[0]+'(.*?)('+$ra[0]+'|'+$ld[0]+')(.*?)'+$rg[0])
  replace = $lqi[0]+'\1'+$lqii[0]+'\2'+$rqii[0]+'\4'+$rqi[0]
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 1")
  # 2. Convert paired “” to «» (but they still could be contained within existing «»)
  # Also non-greedy
  find = Regexp.new($ld[0]+'(.*?)'+$rd[0])
  replace = $lg[0]+'\1'+$rg[0]
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 2")
  # 3. Remaining single “ and ” are mapped to « and »
  # Letter is needed because some “ can be closing 2nd-level quotes
  find = Regexp.new($ld[0]+br($anynonqusp))
  replace = $lg[0]+'\1'
  timer
  $mytext = $mytext.gsub(find, replace)
  find = Regexp.new($rd[0])
  replace = $rg[0]
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 3")
  # 4. Dumb QM (the hardest thing, see explanation below)
  # 4.1 If there are only 2 QM per line, it is obvious
  find = Regexp.new("^(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)$")
  replace = '\1'+$lqi[0]+'\2'+$rqi[0]+'\3'
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 4.1")
  # 4.2 If there are 3 QM, they are obviously nested
  find = Regexp.new("^(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)$")
  replace = '\1'+$lqi[0]+'\2'+$lqii[0]+'\3'+$rqii[0]+$rqi[0]+'\4'
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 4.2")
  # 4.3 If there are 4 QM, there are 2 possibilities
=begin
  # 4.3.0 In multiples of four go nice nested pairs
  n = $anynonqusp
  l = $anynondqu
  p = '(?:'+$punctorspace+')'
  find = Regexp.new("(#{p}|^)#{$dq[0]}((?:(?:#{n}#{l}*)#{$s[0]})?)#{$dq[0]}(#{n}(?:#{l}*#{n})?)#{$dq[0]}((?:#{$anypunct})|(?:#{p}#{l}*#{n})?)#{$dq[0]}")
  replace = '\1'+$lqi[0]+'\2'+$lqii[0]+'\3'+$rqii[0]+'\4'+$rqi[0]
  puts find
  output_from_kasha(om = 0)
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 4.3.0")
=end
  # 4.3.1 Two nested pairs
  find = Regexp.new("^(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)#{$dq[0]}(#{$anynondqu}*)$")
  replace = '\1'+$lqi[0]+'\2'+$lqii[0]+'\3'+$rqii[0]+'\4'+$rqi[0]+'\5'
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 4.3.1")
=begin
  find = Regexp.new("(#{$s[0]}|^)#{$dq[0]}((?:#{n}#{l}*)#{$s[0]})?#{$dq[0]}(#{n}(?:#{l}*#{n})?)#{$dq[0]}((?:#{p}(?:#{l}*#{$s[0]})?#{$dq[0]}#{n}(?:#{l}*#{n})?#{$dq[0]})*)((?:#{p})|(?:#{p}#{l}*#{n})?)#{$dq[0]}")
  replace = '\1'+$lqi[0]+'\2'+$lqii[0]+'\3'+$rqii[0]+'\4\5'+$rqi[0]
# Remove next line after testing
  $horrible = find
  start = Time.now
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 4")
=end
=begin
  # 4.4 Easy and dirty way around remaining cases
  find = Regexp.new("#{$dq[0]}(#{$anyletter}|#{$d[0]})")
  replace = $lqi[0]+'\1'
  timer
  $mytext = $mytext.gsub(find, replace)
  find = Regexp.new("#{$s[0]}#{$dq[0]}")
  replace = $s[0]+$lqi[0]
  $mytext = $mytext.gsub(find, replace)
  find = Regexp.new("(#{$anyletter}|#{$anypunct})#{$dq[0]}")
  replace = '\1'+$rqi[0]
  $mytext = $mytext.gsub(find, replace)
  find = Regexp.new("#{$dq[0]}#{$s[0]}")
  replace = $rqi[0]+$s[0]
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 4.4")
=end
  # 5. Guillemets
  # 5.1 Paired nested guillemets are OK at any place
  find = Regexp.new("(?:#{$lg[0]}|#{$lqi[0]})(#{$anynonqu}*)(?:#{$lg[0]}|#{$lqi[0]})(#{$anynonqu}*)(?:#{$rg[0]}|#{$rqi[0]})(#{$anynonqu}*)(?:#{$rg[0]}|#{$rqi[0]})")
  replace = $lqi[0]+'\1'+$lqii[0]+'\2'+$rqii[0]+'\3'+$rqi[0]
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 5.1")
  # 5.2 Unpaired guillemets
  find = Regexp.new("^(#{$anynonqu}*)(?:#{$lg[0]}|#{$lqi[0]})(#{$anynonqu}*)(?:#{$lg[0]}|#{$lqi[0]})(#{$anynonqu}*)(?:#{$rg[0]}|#{$rqi[0]})(#{$anynonqu}*)$")
  replace = '\1'+$lqi[0]+'\2'+$lqii[0]+'\3'+$rqii[0]+$rqi[0]+'\4'
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 5.2")
  # 5.3 A more difficult case with multiple nests
  # 5.4 Remaining guillemets are replaced with lqi and rqi
  find = Regexp.new("#{$lg[0]}")
  replace = $lqi[0]
  timer
  $mytext = $mytext.gsub(find, replace)
  find = Regexp.new("#{$rg[0]}")
  replace = $rqi[0]
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 5.4")
=begin
  # 6. Spacing aroud quotes
  # 6.1 Space before lqi if not preceded by a paren, bracket or brace
  find = Regexp.new("(?!#{$s[0]}|#{$lp[0]}|#{$lbk[0]}|#{$lbc[0]})#{$lqi[0]}")
  replace = $s[0]+$lqi[0]
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 6.1")
  # 6.2 No space after lqi
  find = Regexp.new("#{$lqi[0]}#{$s[0]}")
  replace = $lqi[0]
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 6.2")
  # 6.3 Space after rqi if not followed by punct
  find = Regexp.new("#{$rqi[0]}(?!#{$anypunct})")
  replace = $rqi[0]+$s[0]
  timer
  $mytext = $mytext.gsub(find, replace)
  showtime("QM step 6.3")
  # 6.4 No space before lqi
#  find = Regexp.new("#{$sp[0]}#{$rqi[0]}")
#  replace = $rqi[0]
#  timer
#  $mytext = $mytext.gsub(find, replace)
#  showtime("QM step 6.4")
=end
end

=begin
How to convert the #{find} in QM step 4" in order to understand anything?

Simple:
#{$dq[0]} — "
#{$s[0]} — space
#{p} — [ ,.?!—-]
#{n} — (?:(?!"| ).)
#{l} — (?:(?!").)

Full:
#{$dq[0]} — ♂dq♀
#{$s[0]} — ♂s♀
#{p} — (?:♂s♀|♂c♀|♂d♀|♂q♀|♂e♀|♂md♀|♂h♀)
#{n} — (?:(?!♂dq♀| ).)
#{l} — (?:(?!♂dq♀).)
=end

# squeeze_spaces
# initial_spaces
quotemarks

output_from_kasha(1, "z.txt")

puts $anynonqu
