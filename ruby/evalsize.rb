mysize = File.stat("mb100.txt").size
mysizekb = mysize/1024
mysizemb = mysize/1048576
if mysize > 10485760 # Files larger than 10 MB should not be allowed 
  puts "Размер файла чересчур велик (#{mysizemb} МБ > 10 МБ). Пожалуйста, отправьте на обработку фрагмент размером менее 10 МБ."
  puts "(И где вы такие громадные тексты берёте? Даже «Война и мир» весит менее 3,5 МБ!)"
end

