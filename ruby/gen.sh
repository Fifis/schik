echo Building test files...

yes "abcdefghijklmnopqrstuvwxyz 123456890 съешь-ка ещё этих мягких французских булочек, да выпей же чаю" | head -c 1000       > kb001.txt
yes "abcdefghijklmnopqrstuvwxyz 123456890 съешь-ка ещё этих мягких французских булочек, да выпей же чаю" | head -c 10000       > kb010.txt
yes "abcdefghijklmnopqrstuvwxyz 123456890 съешь-ка ещё этих мягких французских булочек, да выпей же чаю" | head -c 100000       > kb100.txt
yes "abcdefghijklmnopqrstuvwxyz 123456890 съешь-ка ещё этих мягких французских булочек, да выпей же чаю" | head -c 1000000    > mb001.txt
yes "abcdefghijklmnopqrstuvwxyz 123456890 съешь-ка ещё этих мягких французских булочек, да выпей же чаю" | head -c 10000000 > mb010.txt
yes "abcdefghijklmnopqrstuvwxyz 123456890 съешь-ка ещё этих мягких французских булочек, да выпей же чаю" | head -c 100000000 > mb100.txt

echo Testing...

ruby -v

echo
for i in kb001.txt kb010.txt kb100.txt mb001.txt mb010.txt mb100.txt
do
  echo
  echo "Running: time ruby readlines.rb $i"
  time ruby readlines.rb $i
  echo '---------------------------------------'
  echo "Running: time ruby foreach.rb $i"
  time ruby foreach.rb $i
  echo
done

rm [km]b[01][01][01].txt

