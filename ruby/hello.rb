# hello.rb  
puts 'Hello' 
printf("printf formats numbers like %1.2f, and
strings like %s.\n",3.14156,"me")

puts "What is your name?"
$name = STDIN.gets
puts "Hi "+$name

puts $name.upcase 

require 'unicode'

puts Unicode::upcase($name)

a = 3**2
b = 4**2
Math.sqrt(a+b)

def h(name = "Andreï Костырка")
puts "You are #{name}"
end

class Greet
  def initialize(name = "Andreï Костырка")
    @name = name
  end
  def say_hi
    puts "Приветствую, #{@name}, наперсник разврата!"
  end
  def say_bye
    puts "Изыди же навеки, демон #{@name}!"
  end
end

g = Greet.new("Костырка")

g.respond_to?("name")
g.respond_to?("say_hi")
g.respond_to?("to_s")

class Greet
  attr_accessor :name
end

g = Greet.new("А. В.")
g.respond_to?("name")
g.name="Александр Николаевич"


class MegaGreeter
  attr_accessor :names

  # Create the object
  def initialize(names = "World")
    @names = names
  end

  # Say hi to everybody
  def say_hi
    if @names.nil?
      puts "..."
    elsif @names.respond_to?("each")
      # @names is a list of some kind, iterate!
      @names.each do |name|
        puts "Hello #{name}!"
      end
    else
      puts "Hello #{@names}!"
    end
  end

  # Say bye to everybody
  def say_bye
    if @names.nil?
      puts "..."
    elsif @names.respond_to?("join")
      # Join the list elements with commas
      puts "Goodbye #{@names.join(", ")}.  Come back soon!"
    else
      puts "Goodbye #{@names}.  Come back soon!"
    end
  end

end


if __FILE__ == $0
  mg = MegaGreeter.new
  mg.say_hi
  mg.say_bye

  # Change name to be "Zeke"
  mg.names = "Zeke"
  mg.say_hi
  mg.say_bye

  # Change the name to an array of names
  mg.names = ["Albert", "Brenda", "Charles",
    "Dave", "Engelbert"]
  mg.say_hi
  mg.say_bye

  # Change to nil
  mg.names = nil
  mg.say_hi
  mg.say_bye
end




