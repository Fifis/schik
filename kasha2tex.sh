#!/bin/sh
SwitchFlag () { mv out.txt in.txt; }

sed -e 's/♂s♀/ /g' edited.txt >out.txt; SwitchFlag;
sed -e 's/♂t♀/\t/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂d♀/./g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂c♀/,/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂col♀/:/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂scol♀/;/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂e♀/!/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂q♀/?/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂dq♀/"/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂dol♀/\\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂euro♀/\\euro{}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂pound♀/\\pounds{}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂pc♀/\\%/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂amp♀/\\\&/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂at♀/@/g' in.txt >out.txt; SwitchFlag;
sed -e "s/♂da♀/'/g" in.txt >out.txt; SwitchFlag;
sed -e 's/♂lp♀/(/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rp♀/)/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂lbk♀/[/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rbk♀/]/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂lbc♀/\\{/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rbc♀/\\}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂lg♀/«/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rg♀/»/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂langle♀/‹/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rangle♀/›/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂ld♀/“/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rd♀/”/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂la♀/„/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂ra♀/“/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂ls♀/‘/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rs♀/’/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂ast♀/\*/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂plus♀/\+/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂h♀/-/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂minus♀/\$-\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂nd♀/--/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂md♀/---/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂sl♀/\//g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂bsl♀/\\textbackslash{}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂eq♀/=/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂gr♀/>/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂le♀/</g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂geq♀/\$\\geq\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂leq♀/\$\\leq\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂geqslant♀/\$\\geqslant\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂leqslant♀/\$\\leqslant\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂ldots♀/.../g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂hat♀/\\^{}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂us♀/\\_/g' in.txt >out.txt; SwitchFlag;
sed -e "s/♂ca♀/’/g" in.txt >out.txt; SwitchFlag;
sed -e 's/♂acute♀/´/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂grave♀/`/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂pipe♀/|/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂numero♀/№/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂number♀/\\\#/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂div♀/\$\\div\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂tilde♀/\\~{}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂nbsp♀/~/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂thsp♀/\\,/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂thnb♀/\\,/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂deg♀/\$^\\circ\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂pm♀/\$\\pm\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂times♀/\$\\times\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂cdot♀/\$\\cdot\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂bullet♀/•/g' in.txt >out.txt; SwitchFlag;
sed -e "s/♂prime♀/\${}'\$/g" in.txt >out.txt; SwitchFlag;
sed -e 's/♂pprime♀/\${}''\$/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂sect♀/\\S{}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂copy♀/\\copyright{}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂reg♀/\\textregistered{}/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂tm♀/\\textsuperscript{TM}/g' in.txt >out.txt; SwitchFlag;

sed -e 's/♂rumd♀/ "--- /g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rujd♀/"--~/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rudd♀/"--* /g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂nbh♀/"~/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂bh♀/"=/g' in.txt >out.txt; SwitchFlag;

# Сюда надо будет написать возможность подстановки своих кавычек разных уровней
sed -e 's/♂lqi♀/«/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rqi♀/»/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂lqii♀/„/g' in.txt >out.txt; SwitchFlag;
sed -e 's/♂rqii♀/“/g' in.txt >out.txt; SwitchFlag;

# Restore original sex symbols
sed -e 's/☾m☽/♂/g' in.txt >out.txt; SwitchFlag;
sed -e 's/☾f☽/♀/g' in.txt >out.txt; SwitchFlag;

mv in.txt out.txt
