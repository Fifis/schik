#!/bin/sh
SwitchFlag () { mv edited.txt in.txt; }

## Кавычки

sed -r -e 's/♂s♀♂dq♀(([А-Яа-яЁёA-Za-z0-9]|♂c♀|♂d♀|♂q♀|♂e♀|♂col♀|♂scol♀|♂lp♀|♂rp♀|♂s♀|♂h♀)*)♂s♀♂dq♀(([А-Яа-яЁё]|♂s♀)*)♂dq♀/♂s♀♂lqi♀\1♂s♀♂lqii♀\3♂rqii♀♂rqi♀/g' in.txt >edited.txt; SwitchFlag; # Три прямые кавычки с правильными пробелами меняем на двухуровневые
sed -r -e 's/♂dq♀([А-Яа-яЁё0-9.()])(.*)([А-Яа-яЁё0-9.!?%()])♂dq♀/♂lqi♀\1\2\3♂rqi♀/g' in.txt >edited.txt; SwitchFlag; # Текст вида |"что-то в кавычках"| с правильными пробелами, причём содержимое кавычек может начинаться с точки: |в письме значилось: "...и не забудьте покормить кота!"|
sed -r -e 's/♂dq♀(.*)♂dq♀/♂lqi♀\1♂rqi♀/g' in.txt >edited.txt; SwitchFlag; # Текст вида |знаете," мне начхать " на пробелы|

# Обработка наполовину правильных кавычек
/usr/bin/perl -p -i -e 's/♂lg♀(((?!♂rg♀).)*)♂lg♀(.*)♂rg♀(((?!♂lg♀).)*)♂rg♀/♂lqi♀$1♂lqii♀$3♂rqii♀$4♂rqi♀/gm' in.txt
/usr/bin/perl -p -i -e 's/♂lg♀(((?!♂rg♀).)*)♂lg♀/♂lqi♀$1♂lqii♀/gm' in.txt
/usr/bin/perl -p -i -e 's/♂rg♀(((?!♂lg♀).)*)♂rg♀/♂rqii♀$1♂rqi♀/gm' in.txt
sed -r -e 's/(♂s♀|♂nbsp♀)+(♂rg♀|♂rp♀|♂ra♀)/\1/g' in.txt >edited.txt; SwitchFlag; # Удаление неправильных пробелов рядом с кавычками, отличных от прямых
sed -r -e 's/(♂lg♀|♂lp♀|♂la♀) +/\1/g' in.txt >edited.txt; SwitchFlag;
# Дочистить остающиеся одинокие кавычки в соответствии с прилегающими пробелами
sed -r -e 's/([А-ЯЁа-яё0-9!?.])♂dq♀/\1♂rqi♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/♂dq♀([.,?!]|♂s♀|♂nbsp♀)/♂rqi♀\1/g' in.txt >edited.txt; SwitchFlag; # Грубое решение для тех, кто не прошёл предыдущий пункт (неверная пунктуация)
sed -r -e 's/(♂s♀|♂nbsp♀)♂dq♀/\1♂lqi♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/♂dq♀(.*)♂dq♀/♂lqi♀\1♂rqi♀/g' in.txt >edited.txt; SwitchFlag; # Обработка оставшихся прямых кавычек
sed -r -e 's/^(.*)♂dq♀/\1♂lqi♀/g' in.txt >edited.txt; SwitchFlag; # Оставшиеся непарные прямые кавычки станут для простоты левыми и обязательно отсигналит в счётчик закрытых-открытых кавычек
# Сконвертировать необработанные, но предположительно хорошие кавычки в финальные
sed -r -e 's/♂lg♀/♂lqi♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/♂rg♀/♂rqi♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/♂la♀/♂lqii♀/g' in.txt >edited.txt; SwitchFlag; # Если во входном тексте нашлись «лапки»
sed -r -e 's/♂ra♀/♂rqii♀/g' in.txt >edited.txt; SwitchFlag;
# Сконвертировать остающиеся после замен полуправильные конструкции из лапок
/usr/bin/perl -p -i -e 's/♂lqi♀(((?!♂rqi♀).)*)♂lqi♀(.*)♂rqi♀(((?!♂lqi♀).)*)♂rqi♀/♂lqi♀$1♂lqii♀$3♂rqii♀$4♂rqi♀/gm' in.txt # Копия команды сверху с (l|r)(q|a), заменёнными на (l|r)(qi|qii)


## Тире
sed -r -e 's/([0-9])(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*-(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*([0-9])/\1♂nd♀\2/g' in.txt >edited.txt; SwitchFlag; # Среднее тире в числовых диапазонах
sed -r -e 's/([IVXDCM])(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*-(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*([IVXDCM])/\1♂nd♀\2/g' in.txt >edited.txt; SwitchFlag; # Среднее тире в римских числовых диапазонах
sed -r -e 's/(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)+♂md♀(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)+/♂rumd♀/g' in.txt >edited.txt; SwitchFlag; # Русское внутритекстовое длинное тире
sed -r -e 's/([а-яё])♂md♀(А-ЯЁ)/\1♂rujd♀\2/g' in.txt >edited.txt; SwitchFlag; # Русское межименное длинное тире
sed -r -e 's/^(♂s♀|♂nbsp♀)*(♂md♀|♂nd♀|♂h♀)(♂s♀|♂nbsp♀)*/\1♂rudd♀\3/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/(♂s♀|♂nbsp♀)*(♂md♀|♂nd♀|♂h♀)$/♂rumd♀/g' in.txt >edited.txt; SwitchFlag; # Свисающий с конца строки дефис

## Пробелы при прочей пунктуации
sed -r -e 's/(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)(♂с♀|♂d♀|♂e♀|♂q♀|♂col♀|♂scol♀)/\2/g' in.txt >edited.txt; SwitchFlag; # Пробелы перед [.,!?;:]
sed -r -e 's/([а-яё](♂с♀|♂d♀|♂e♀|♂q♀|♂col♀|♂scol♀){1,5})([А-ЯЁ0-9])/\1♂s♀\3/g' in.txt >edited.txt; SwitchFlag; # Забытые пробелы после [.,?!;:]
/usr/bin/perl -p -i -e 's/(?<!♂dq♀♂rqi♀♂rg♀♂ld♀♂rd♀)(♂с♀|♂e♀|♂q♀|♂col♀|♂scol♀)([a-zA-ZА-Яа-яёЁ])/$1♂s♀$2/gm' in.txt
# Это было как str = repL(str, "([^\"»“”][,!;:?])([a-zA-ZА-Яа-яёЁ])", "$1 $2", "punctum_space");
# Выяснить, зачем нужно исключение кавычек
sed -r -e 's/♂s♀(♂с♀|♂e♀|♂q♀|♂col♀|♂scol♀|♂rp♀)♂s♀/\1♂s♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/(♂s♀)+♂h♀(♂s♀)+(то|либо|нибудь|небудь)/♂bh♀\3/g' in.txt >edited.txt; SwitchFlag;
# Тут надо восстановить дефисы в местоимениях и прочих коротких дефисных словах, которые превратились в тире (или таковую замену сделать ещё до замен тире)
# Тут будет идти команда, создающая мягкие переносы с составных словах
/usr/bin/perl -p -i -e 's/(где|как(?:ая|и(?:[емх]|ми)|о(?:го|[ей]|му)|ую)?|ке[йм]|ко(?:г(?:да|о)|му)|кто|куда|че(?:го|[йм]|му)|чьи[мх]?|чьими|чь[юя])(?:♂s♀)*♂h♀(?:♂s♀)*(то|либо|нибудь|небудь)/$1♂h♀$2/gm' in.txt
sed -r -e 's/([А-ЯЁа-яё0-9A-Za-z])(♂lp♀|♂lg♀|♂lqi♀)/\1♂s♀\2/g' in.txt >edited.txt; SwitchFlag; # Пробел перед открывающей скобкой или кавычкой
sed -r -e 's/(♂rp♀|♂rg♀|♂rqi♀)([А-ЯЁа-яё0-9A-Za-z])/\1♂s♀\2/g' in.txt >edited.txt; SwitchFlag;


## Общепринятые сокращения
sed -r -e 's/(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)+([гГвВ]{1,3})((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)н((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)э(♂d♀)?/♂nbsp♀\2♂d♀♂nbsp♀♂nery♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)+([гГ]од(а|у|ом|ы|ов|ам|ами|ах)?|[вВ]ек(а|у|ом|е|ов|ам|ами|ах)?)((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)н((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)э(♂d♀)?/♂nbsp♀\2♂nbsp♀♂nery♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)+([гГвВ]{1,3})((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)до(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*н((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)э(♂d♀)?/♂nbsp♀\2♂d♀♂s♀♂donery♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)+([гГ]од(а|у|ом|ы|ов|ам|ами|ах)?|[вВ]ек(а|у|ом|е|ов|ам|ами|ах)?)((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)до(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*н((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)э(♂d♀)?/♂nbsp♀\2♂s♀♂donery♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*н((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)э(♂d♀)?/♂nbsp♀♂nery♀/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/(♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*н((♂d♀|♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*)э(♂q♀|♂e♀|♂c♀|♂col♀|♂scol♀)/♂nbsp♀♂nery♀\1/g' in.txt >edited.txt; SwitchFlag;

sed -r -e 's/т[.](♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*п[.]/♂tompod♀\1/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/т[.](♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*е[.]/♂toest♀\1/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/т[.](♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*д[.]/♂takdal♀\1/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/т[.](♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*н[.]/♂taknaz♀\1/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/т[.](♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*к[.]/♂takkak♀\1/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/т[.](♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*ч[.]/♂tomchi♀\1/g' in.txt >edited.txt; SwitchFlag;
sed -r -e 's/с[.](♂s♀|♂nbsp♀|♂thsp♀|♂thnb♀)*г[.]/♂sgoda♀\1/g' in.txt >edited.txt; SwitchFlag;





mv in.txt edited.txt

