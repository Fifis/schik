SCHIK TYPOGRAPHING AND PROOFING TOOL
====================================

“SchiK” punctuation checking tool (after “SCHirykalov” and “Kostyrka”)
in a free set of automated replacement scripts for ennobling electronic texts
(punctuation spacing, hard ties for prepositions, punctuation error
correction etc.).

“SchiK” is written in Perl and utilizes POSIX regular expressions. The inner
syntax is governed by “KASchA” language specification that is fully
documented in the corresponging KASchA.md readme file.
This tool comes with absolutely no warranty under CC BY-SA license.


Version history:
----------------

* 0.7 (scheduled) --- text processor rewritten and split in three ones:
(1) obtaining the input and writing it to server; (2) text conversion using
the “KASchA” typographic syntax with control sequences for punctuation and
spacing, and the very processing itself; (3) conversion to selected ouput
format and delivery of the result.

* 0.6 --- First complete JavaScript implementation sketched, all intended
autoreplacement rules work, except for the non-breakable space due to the
limitations levied by the use of HTML textareas notable for suppressing all
nbsp’s. This is the first public alpha version, to be further improved by
by rewriting the input reading using Perl.


ТИПОГРАФ «ШИК»
==============

Типограф «ШиК» («Ширыкалов и Костырка») — это открытый и свободный сервис
автозамены для облагораживания текстов (корректура пробелов при знаках
препинания, привязка предлогов, исправление пунктуационных ошибок и многое
другое).

Почти весь «ШиК» — это работа регулярных выражений sed extended и Perl.
Программа использует специально созданный для целей облагораживания
текстов язык пунктуации «КАША», спецификация которого задокументирована
в файле KASchA.md. Скрипты, используемые в данном сервисе, предоставляются
безо всякой гарантии по лицензии CC BY-SA.

Функции
-------
* Удаление повторных пробельных символов и замена нестандартных пробельных 
знаков (табуляции, неразрывные, тонкие, неразрывные тонкие) на простые 
пробелы;
* Исправление кавычек (включая «вложенные „второго уровня“»);

История изменений
-----------------

* 0.7 (планируется) --- переписана часть, отвечающая за обработку текстов:
функция обработки разделена на три: (1) чтение входного текста и отправка
оного на сервер; (2) конвертирование текста в язык «КАША» с условными 
командами для знаков препинания и служебных элементов текста, обработка
блока текста средствами типографа; (3) конвертирование текста в выходной
формат, указанный пользователем, и вывод его на страницу в качестве
результата.

* 0.6 (2014/03/28) --- выпущена первая реализация на JavaScript’е, работают все
запланированные автозамены, кроме неразрывного пробела (из-за ограничений,
касающихся выдаваемого в textarea’х, в котором подавляются все nbsp). Это
первая публичная альфа-версия, на основе которой и будет создан Perl-клиент,
и последняя, в которой тексты облагораживаются на стороне клиента.
